Installation Instructions
======================

Dora can be installed through Eclipse's update mechanism. Dora is distributed under the terms of the Eclipse Public License, v. 1.0.

System requirements: 
------------------

Eclipse 3.4, Java 1.5, and dot. Dora has been developed on Linux, and has been tested on Mac OS X and Windows.

Installation Steps
-----------------

1. From the Eclipse menu bar, select Help | Software Updates | Available Software
2. Select "Add Site..."
3. Enter in the URL for this page, "http://www.cis.udel.edu/~gibson/dora/download/" and hit OK
4. Select the Dora plug-in and hit Install..., then click Next
5. Accept the terms of the license agreement and click Finish
6. Restart your workspace
7. Go to Window > Preferences > Dora and make sure the 'dot:' preference points to your installation. This is set to /usr/bin/dot by default. You can download dot, which is part of graphviz, here.

Alternate Installation
-------------------

If you have trouble installing Dora directly form the update site, you can download the contents of the site and load it locally.
 

Usage Directions
=================

1. Select a method in the editor and right click its name
2. Select "Explore with Dora" from the drop-down menu
  - The first time a query is performed on a project, some initialization takes place
4. Enter in your query and hit "Enter"
5. The results are graphically displayed in a separate window. The bold method is the starting point, solid line methods are considered relevant, and methods with dashed lines are less relevant.
6. The threshold for considering a method to be relevant and less relevant, as well as the maximum number of edges away from the seed can be set in the preferences.
7. Note There is an inherent relationship between the number of elements returned and the max number of edges that affects readability. That is, the lower the exploration threshold, typically the lower the max edges will need to be to preserve readability.

Troubleshooting
--------------

If Dora is taking too long to run, consider giving eclipse access to more memory. 
When executing eclipse, pass the argument: '-vmargs -Xmx512M' and change 512 to 1024 if you have 2GB of RAM, or 2048 if you have 3GB, etc.

If you are having trouble getting dot to display the relevant window inside Eclipse, you can still view the output by running dot manually on $workspace/.metadata/.plugins/Dora/rw.dot.